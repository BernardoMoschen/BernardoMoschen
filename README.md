<h2> Welcome! :bowtie: </br>
 My name is Bernardo Moschen, I'm a Student at Unisinos, based in Porto Alegre/RS - Brazil.
 I love Python and everything that comes to coding.
</h2>


<h3> 👨🏻‍💻 About Me </h3>

- 🔍 &nbsp; Seeking to expand my knowledge.
- 🎓 &nbsp; Studying **Analysis and Systems Development**, scheduled for completion in 2022.
- 💼 &nbsp; I currently work as an Accounting Assistant and I'm looking for my first opportunity in the technology area.
- 📚 &nbsp; Focusing on **Python**.
- 🗺️ &nbsp; I love to read and travel in my free time.
- 🌱 &nbsp; My final career goal is in the field of **artificial intelligence, but I am open to everything**.
- 💬 &nbsp; Feel totally free to interact with me!
- 🔗 &nbsp; See more about me on <a href="https://linktr.ee/BernardoMoschen">Linktree</a>

<h3>🧰 Stack</h3>

- 💻 &nbsp; Python | Java
- 🌐 &nbsp; HTML | JavaScript - very little, to be honest.
- 🔧 &nbsp; Git

<br/>

<i>Follow me:</i><br>

[![Gmail](https://img.shields.io/badge/-GMAIL-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:bernardomoschen.dev@gmail.com)
[![LinkedIn](https://img.shields.io/badge/-LINKEDIN-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/bernardomoschen/)
